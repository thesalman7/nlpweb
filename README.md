# nlpweb application
1. Data borrowed from https://www.kaggle.com/, unfortunately didn't save the exact link. Now lost the track (Will update soon).
2. Trains the model for existing book titles/names.
3. Based on the training model, the app classifies existing titles. The titles have been classified into 25 groups, based on genre available on the internet.
    
    a. One book title can fall into multiple groups
4. User can add new titles.
5. The newly added title will automatically will be classified in the groups.
6. Once the titles has been added successfully, user also receives recommendation based on the newly added titles.
7. User can also view number of books in each group.
8. Book title are also click-able, to show all the groups the particular title falls in.
9. User can also export the data in XML/CSV(Excel).
    
    a. All data
    
    b. Only author
    
    c. Only Title

10. Currently showing only last 2000 records out of ~670000. As datatables couldn't handle the large data.
   
