FROM ubuntu:18.04
#FROM python:3.7

RUN apt-get update \
   && apt-get install python3 -y \
   && apt-get install -y python3-pip python3-dev \
   && cd /usr/local/bin \
   #&& ln -s /usr/bin/python3 python \
   && pip3 install --upgrade pip

COPY ./requirements.txt /app/requirements.txt

RUN pip3 install -r /app/requirements.txt


COPY . /app


EXPOSE 8888

CMD [ "python3", "app.py"]