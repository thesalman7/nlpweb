import gensim
import nltk
from flask import Flask, request, render_template, send_file
import numpy as np
from models import Schema
from helper import json_error, json_success, validation_errors
from service import Books, ExportData, classify_book, fetch_all_titles, preprocess, \
    get_paginated_list, grouping_books, fetch_group_basis

app = Flask(__name__)


@app.route("/", methods=["GET"])
# description="Index page, will list the books and on top will be a form to add new books"
def list_books():
    try:
        """ Fetching list from DB - Start """
        books_list = Books().list()
        """ Fetching list from DB - End """

        response = {
                        "data": books_list,
                        "suggestion_flag": False,
                        "suggested_books": [],
                        "data_on_screen": len(books_list),
                        "total_data": Books().total_records()
                    }
        return render_template("index.html", results=response)
    except Exception as e:
        return json_error(message=str(e))


@app.route("/books", methods=["POST"])
# description="New books will be added using this URL"
def add_book():
    try:
        params = request.get_json()

        """ POST Parameters Validation - Start """
        if not params:
            params = request.form

        if not params.get("title") or not params.get("author"):
            return validation_errors(errors="title and author is required")
        """ POST Parameters Validation - End """

        """ Creating Groups for newly added book - Start """
        groups = classify_book(params["title"])
        """ Creating Groups for newly added book - End """

        """ Adding new book in DB - Start """
        temp_params = {
            "title": request.form["title"],
            "author": request.form["author"],
            "groups": groups
        }
        books_list = Books().create(temp_params)
        """ Adding new book in DB - End """

        """ Fetching recommended books - Start """
        recommended_books = Books().list_books_in_group(groups.split(", ")[0])
        """ Fetching recommended books - End """

        response = {
            "data": books_list,
            "suggestion_flag": True,
            "suggested_books": recommended_books,
            "data_on_screen": len(books_list),
            "total_data": Books().total_records()
        }
        return render_template("index.html", results=response)
    except Exception as e:
        return json_error(message=str(e))


@app.route("/books/<item_id>", methods=["DELETE", "POST", "GET"])
# description="A title will be removed from the database, the status will be set to 0"
def delete_book(item_id):
    try:
        books_list = Books().delete(item_id)
        response = {
            "data": books_list,
            "suggestion_flag": False,
            "suggested_books": [],
            "data_on_screen": len(books_list),
            "total_data": Books().total_records()
        }
        return render_template("index.html", results=response)
    except Exception as e:
        return json_error(message=str(e))


@app.route("/groups", methods=["GET"])
# description="New books will be added using this URL"
def get_group_wise_books():
    try:
        """ Fetching all the books from DB - Start """
        books_list = Books().list()
        """ Fetching all the books from DB - End """

        """ Grouping of books - Start """
        data = grouping_books(books_list)
        records = []
        groups = data.keys()
        for grp in groups:
            temp = {"groups": int(grp), "books_count": data[grp]}
            records.append(temp)
        """ Grouping of books - End """

        """ Sorting groups - Start """
        if records:
            records = sorted(records, key=lambda i: i['books_count'], reverse=True)
        """ Sorting groups - Start """

        return render_template("groups.html", results=records)
    except Exception as e:
        return json_error(message=str(e))


@app.route("/export-excel/<export_type>", methods=["GET"])
# description="This URL will export excel data"
def export_excel(export_type):
    ExportData().export_csv(export_type)
    return send_file("static/export_data.csv", as_attachment=True, attachment_filename="books_data.csv")


@app.route("/export-xml/<export_type>", methods=["GET"])
# description="This URL will export XML data"
def export_xml(export_type):
    ExportData().export_xml(export_type)
    return send_file("static/export_data.xml", as_attachment=True, attachment_filename="books_data.xml")


@app.route("/train-model", methods=["GET"])
# description="Training for classifying book on titles"
def train_book_model():

    """ Fetching all the documents i.e. book titles  - Start """
    documents = fetch_all_titles()
    """ Fetching all the documents i.e. book titles - End """

    np.random.seed(2018)

    """ Downloading package wordnet  - Start """
    nltk.download('wordnet')
    """ Downloading package wordnet - End """

    processed_docs = documents['title'].map(preprocess)
    dictionary = gensim.corpora.Dictionary(processed_docs)
    dictionary.filter_extremes(no_below=15, no_above=0.5, keep_n=100000)
    bow_docs = [dictionary.doc2bow(doc) for doc in processed_docs]

    """ Running LDA using Bag of Words - Start """
    lda_model = gensim.models.LdaMulticore(bow_docs, num_topics=25, id2word=dictionary, passes=2, workers=2)
    lda_model.save('trained_model/lda_train_bow.model')
    """ LDA Training - End """

    return json_success(results="Training Complete, files saved!")


@app.route("/group-basis", methods=["GET"])
def group_basis():
    try:
        data = fetch_group_basis()
        return render_template("group_basis.html", results=data)
    except Exception as e:
        return json_error(message=str(e))


if __name__ == "__main__":
    try:
        Schema()
    except Exception as e:
        print(str(e))
    app.run(debug=True, port=5000)
